package controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import sun.security.util.Password;

public class loginController {


    @FXML
    private TextField loginTextField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Button loginButton;

    public void start(Stage loginPage) {

        loginButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {

                loginEvent();
            }
        });

    }


    /**
     * Handles event of the login button being clicked.
     */
    private void loginEvent() {

        boolean didPassEmptyCheck = true;

        if(loginTextField.getText().isEmpty()) {
            Alert invalidLogin = new Alert(Alert.AlertType.WARNING);
            invalidLogin.setTitle("Empty Field");
            invalidLogin.setHeaderText("Empty username field.");
            invalidLogin.setContentText("Try again with a valid username.");
            invalidLogin.showAndWait();
            didPassEmptyCheck = false;
        } else if(passwordField.getText().isEmpty()) {
            Alert invalidPassword = new Alert(Alert.AlertType.WARNING);
            invalidPassword.setTitle("Empty Field");
            invalidPassword.setHeaderText("Empty password field.");
            invalidPassword.setContentText("Try again with a valid password.");
            invalidPassword.showAndWait();
            didPassEmptyCheck = false;
        }

        if(didPassEmptyCheck == true) {
            if(loginTextField.getText().compareTo("admin") == 0) {
                System.out.println("AdminCheck");
            } else {
                System.out.println("UserCheck");
            }
        }



    }


}
