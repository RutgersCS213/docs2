package view;

import controller.loginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../controller/loginScreen.fxml"));

        AnchorPane root = (AnchorPane)loader.load();

        loginController login = loader.getController();
        login.start(primaryStage);

        Scene scene = new Scene(root, 600, 400);

        primaryStage.setTitle("Account Login");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);


        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
